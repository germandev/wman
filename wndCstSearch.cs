﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WMan_Werkstatt_Management
{
    public partial class wndCstSearch : Form
    {
        static List<string[]> gridResult;
        wndMain wndMdiParent;

        public wndCstSearch(wndMain mdiParent)
        {
            InitializeComponent();
            wndMdiParent = mdiParent;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            resultGrid.Rows.Clear();
            List<string[]> resultData = sqlLite.searchCustomer(tbPlate.Text, tbLastName.Text, tbPhone.Text);
            if (resultData == null)
                MessageBox.Show("Es wurden keine Angabgen gemacht, nach was gesucht werden soll!", "WMan - Das Werkstatt - Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            else if (resultData.Count == 0)
            {
                Logging.LogInfo("Die Suche lieferte keine Ergebnisse...");
                MessageBox.Show("Es konnten kein Kunde zu den angegebenen Daten gefunden werden", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
            {
                gridResult = resultData;
                foreach (string[] item in resultData)
                    resultGrid.Rows.Add(new object[] { item[0], item[1], item[2], item[3], item[4] });
            }
        }

        private void checkKey(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnSearch_Click(sender, e);
        }

        private void resultGrid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (resultGrid.SelectedRows.Count == 1)
            {
                int cstID = Convert.ToInt32(gridResult[resultGrid.SelectedRows[0].Index][5]);
                Logging.LogInfo("Erstelle \"Kundenübersicht-Fenster\"...");
                wndCstOverview cstWindow = new wndCstOverview(cstID);
                cstWindow.MdiParent = wndMdiParent;
                Logging.LogInfo("Zeige \"Kundenübersicht-Fenster\" an...");
                cstWindow.Show();
                
            }
        }
    }
}
