﻿using System;
using System.IO;
using System.Data.SQLite;
using System.Collections.Generic;

namespace WMan_Werkstatt_Management
{
    public static class sqlLite
    {
        static SQLiteConnection dbConnection;
        const string dbFile = @"database\wman.db";
        public static void Init()
        {
            try
            {
                if (File.Exists(dbFile))
                {
                    Logging.LogInfo("Datenbank gefunden...");
                    dbConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", dbFile));
                    Logging.LogInfo("Datenbank wird geöffnet...");
                    dbConnection.Open();
                    Logging.LogInfo("Datenbank erfolgreich geöffnet...");
                }
                else
                {
                    Logging.LogError("Die Datenbank konnte nicht gefunden werden!");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
            }
            catch (Exception ex)
            {
                Logging.LogError(ex.Message);
                Console.ReadKey();
            }
        }

        internal static List<string[]> searchCustomer(string plate, string lastName, string phone, int cstID = -1)
        {
            Logging.LogInfo("Kundensuche gestartet...");
            string sqlQuery = string.Empty;
            List<string[]> sqlTableResult = null;
            if (!string.IsNullOrWhiteSpace(plate))
                sqlQuery = "SELECT * FROM customer"; //ToDo
            else if (!string.IsNullOrWhiteSpace(lastName))
                sqlQuery = string.Format("SELECT * FROM customer WHERE LastName = '{0}' ORDER BY FirstName ASC", lastName);
            else if (!string.IsNullOrWhiteSpace(phone))
                sqlQuery = string.Format("SELECT * FROM customer WHERE Phone = '{0}'", phone);
            else if (cstID > -1)
                sqlQuery = string.Format("SELECT * FROM customer WHERE ID = {0}", cstID);
            if (string.IsNullOrWhiteSpace(sqlQuery))
            {
                Logging.LogWarning(string.Format("Abbruch der Suche, es wurde kein Parameter angegeben..."));
                return sqlTableResult;
            }
            else
            {
                sqlTableResult = new List<string[]>();
                SQLiteCommand SQLCommand = new SQLiteCommand(sqlQuery, dbConnection);
                SQLiteDataReader SQLReader = SQLCommand.ExecuteReader();
                string[] currentData;
                while (SQLReader.Read())
                {
                    Logging.LogInfo(string.Format("Kunde {0}, {1} gefunden...", SQLReader["LastName"], SQLReader["FirstName"]));
                    currentData = new string[] { SQLReader["LastName"].ToString(), SQLReader["FirstName"].ToString(), SQLReader["Address"].ToString(), SQLReader["ZIP"].ToString(), SQLReader["Town"].ToString(), SQLReader["ID"].ToString() };
                    sqlTableResult.Add(currentData);
                }
                SQLReader.Close();
                Logging.LogInfo("Kundensuche erfolgreich beendet...");
                return sqlTableResult;
            }
        }

        internal static int CreateNewCustomer(string firstName, string lastName, string Address, string ZIP, string Town, string Phone, string Mail)
        {
            int count = 0;
            Logging.LogInfo("Die Anlage eines neuen Kunden wird gestartet...");
            Logging.LogInfo("Es wird überprüft ob bereits ein Kunde mit diesen Daten existiert...");
            string SQLQuery = string.Format("SELECT ID FROM customer WHERE LastName = '{0}' AND FirstName = '{1}' AND Address = '{2}'", lastName, firstName, Address);
            SQLiteCommand SQLCommand = new SQLiteCommand(SQLQuery, dbConnection);
            SQLiteDataReader SQLReader = SQLCommand.ExecuteReader();
            while (SQLReader.Read())
                count++;
            if(count > 0)
            {
                SQLReader.Close();
                return -1;
            }
            else
            {
                SQLQuery = string.Format("INSERT INTO customer (LastName, Firstname, Address, ZIP, Town, Phone, Mail, CreatedAt) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\", \"{6}\", {7})", lastName, firstName, Address, ZIP, Town, Phone, Mail, (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
                SQLCommand = new SQLiteCommand(SQLQuery, dbConnection);
                SQLCommand.ExecuteNonQuery();
                Logging.LogInfo("Der Kunde wurde erfolgreich angelegt...");
                return 0; //ToDo retun last insert ID
            }
        }
    }
}
