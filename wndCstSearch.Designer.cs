﻿namespace WMan_Werkstatt_Management
{
    partial class wndCstSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(wndCstSearch));
            this.boxSearch = new System.Windows.Forms.GroupBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.lblLastname = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.lblPlate = new System.Windows.Forms.Label();
            this.tbPlate = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.boxResults = new System.Windows.Forms.GroupBox();
            this.resultGrid = new System.Windows.Forms.DataGridView();
            this.cLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cZIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boxSearch.SuspendLayout();
            this.boxResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // boxSearch
            // 
            this.boxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.boxSearch.Controls.Add(this.lblPhone);
            this.boxSearch.Controls.Add(this.tbPhone);
            this.boxSearch.Controls.Add(this.lblLastname);
            this.boxSearch.Controls.Add(this.tbLastName);
            this.boxSearch.Controls.Add(this.lblPlate);
            this.boxSearch.Controls.Add(this.tbPlate);
            this.boxSearch.Location = new System.Drawing.Point(4, 0);
            this.boxSearch.Name = "boxSearch";
            this.boxSearch.Size = new System.Drawing.Size(532, 107);
            this.boxSearch.TabIndex = 0;
            this.boxSearch.TabStop = false;
            this.boxSearch.Text = "Suche";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(12, 74);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(83, 13);
            this.lblPhone.TabIndex = 5;
            this.lblPhone.Text = "Telefonnummer:";
            // 
            // tbPhone
            // 
            this.tbPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPhone.Location = new System.Drawing.Point(113, 71);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(413, 20);
            this.tbPhone.TabIndex = 4;
            this.tbPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.checkKey);
            // 
            // lblLastname
            // 
            this.lblLastname.AutoSize = true;
            this.lblLastname.Location = new System.Drawing.Point(12, 48);
            this.lblLastname.Name = "lblLastname";
            this.lblLastname.Size = new System.Drawing.Size(62, 13);
            this.lblLastname.TabIndex = 3;
            this.lblLastname.Text = "Nachname:";
            // 
            // tbLastName
            // 
            this.tbLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLastName.Location = new System.Drawing.Point(113, 45);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(413, 20);
            this.tbLastName.TabIndex = 2;
            this.tbLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.checkKey);
            // 
            // lblPlate
            // 
            this.lblPlate.AutoSize = true;
            this.lblPlate.Location = new System.Drawing.Point(12, 22);
            this.lblPlate.Name = "lblPlate";
            this.lblPlate.Size = new System.Drawing.Size(72, 13);
            this.lblPlate.TabIndex = 1;
            this.lblPlate.Text = "Kennzeichen:";
            // 
            // tbPlate
            // 
            this.tbPlate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPlate.Enabled = false;
            this.tbPlate.Location = new System.Drawing.Point(113, 19);
            this.tbPlate.Name = "tbPlate";
            this.tbPlate.Size = new System.Drawing.Size(413, 20);
            this.tbPlate.TabIndex = 0;
            this.tbPlate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.checkKey);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(542, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(78, 103);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Suchen";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // boxResults
            // 
            this.boxResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.boxResults.Controls.Add(this.resultGrid);
            this.boxResults.Location = new System.Drawing.Point(4, 113);
            this.boxResults.Name = "boxResults";
            this.boxResults.Size = new System.Drawing.Size(616, 226);
            this.boxResults.TabIndex = 2;
            this.boxResults.TabStop = false;
            this.boxResults.Text = "Ergebnisse";
            // 
            // resultGrid
            // 
            this.resultGrid.AllowUserToAddRows = false;
            this.resultGrid.AllowUserToDeleteRows = false;
            this.resultGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.resultGrid.BackgroundColor = System.Drawing.Color.White;
            this.resultGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cLastName,
            this.cFirstName,
            this.cAddress,
            this.cZIP,
            this.cTown});
            this.resultGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.resultGrid.Location = new System.Drawing.Point(8, 19);
            this.resultGrid.MultiSelect = false;
            this.resultGrid.Name = "resultGrid";
            this.resultGrid.ReadOnly = true;
            this.resultGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultGrid.ShowEditingIcon = false;
            this.resultGrid.Size = new System.Drawing.Size(602, 201);
            this.resultGrid.TabIndex = 0;
            this.resultGrid.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.resultGrid_CellContentDoubleClick);
            // 
            // cLastName
            // 
            this.cLastName.HeaderText = "Nachname";
            this.cLastName.Name = "cLastName";
            this.cLastName.ReadOnly = true;
            this.cLastName.Width = 84;
            // 
            // cFirstName
            // 
            this.cFirstName.HeaderText = "Vorname";
            this.cFirstName.Name = "cFirstName";
            this.cFirstName.ReadOnly = true;
            this.cFirstName.Width = 74;
            // 
            // cAddress
            // 
            this.cAddress.HeaderText = "Adresse";
            this.cAddress.Name = "cAddress";
            this.cAddress.ReadOnly = true;
            this.cAddress.Width = 70;
            // 
            // cZIP
            // 
            this.cZIP.HeaderText = "PLZ";
            this.cZIP.Name = "cZIP";
            this.cZIP.ReadOnly = true;
            this.cZIP.Width = 52;
            // 
            // cTown
            // 
            this.cTown.HeaderText = "Stadt";
            this.cTown.Name = "cTown";
            this.cTown.ReadOnly = true;
            this.cTown.Width = 57;
            // 
            // wndCstSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 343);
            this.Controls.Add(this.boxResults);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.boxSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "wndCstSearch";
            this.Text = "Kundensuche";
            this.boxSearch.ResumeLayout(false);
            this.boxSearch.PerformLayout();
            this.boxResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox boxSearch;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.Label lblLastname;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label lblPlate;
        private System.Windows.Forms.TextBox tbPlate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox boxResults;
        private System.Windows.Forms.DataGridView resultGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn cZIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTown;
    }
}