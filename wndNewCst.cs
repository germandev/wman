﻿using System;
using System.Windows.Forms;

namespace WMan_Werkstatt_Management
{
    public partial class wndNewCst : Form
    {
        public wndNewCst()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Logging.LogInfo("Ein neuer Kunde soll angelegt werden...");
            if(string.IsNullOrWhiteSpace(tbFirstname.Text))
            {
                Logging.LogWarning("Der Kunde konnte nicht angelegt werden, da der Vorname fehlt!");
                MessageBox.Show("Der Vorname des Kunden fehlt!", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                tbFirstname.Focus();
            }
            else
            {
                Logging.LogInfo("Der Vorname des Kunden scheint ok zu sein...");
                if(string.IsNullOrWhiteSpace(tbLastname.Text))
                {
                    Logging.LogWarning("Der Kunde konnte nicht angelegt werden, da der Nachname fehlt!");
                    MessageBox.Show("Der Nachname des Kunden fehlt!", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    tbLastname.Focus();
                }
                else
                {
                    Logging.LogInfo("Der Nachname des Kunden scheint ok zu sein...");
                    if(string.IsNullOrWhiteSpace(tbAddress.Text))
                    {
                        Logging.LogWarning("Der Kunde konnte nicht angelegt werden, da die Adresse fehlt!");
                        MessageBox.Show("Die Adresse des Kunden fehlt!", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        tbAddress.Focus();
                    }
                    else
                    {
                        Logging.LogInfo("Die Adresse des Kunden scheint ok zu sein...");
                        if(string.IsNullOrWhiteSpace(tbZIP.Text))
                        {
                            Logging.LogWarning("Der Kunde konnte nicht angelegt werden, da die PLZ fehlt!");
                            MessageBox.Show("Die PLZ des Kunden fehlt!", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            tbZIP.Focus();
                        }
                        else
                        {
                            Logging.LogInfo("Die PLZ des Kunden scheint ok zu sein...");
                            if(string.IsNullOrWhiteSpace(tbTown.Text))
                            {
                                Logging.LogWarning("Der Kunde konnte nicht angelegt werden, da der Ort fehlt!");
                                MessageBox.Show("Der Ort des Kunden fehlt!", "WMan - Das Werkstatt-Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                tbTown.Focus();
                            }
                            else
                            {
                                int returnCode = sqlLite.CreateNewCustomer(tbFirstname.Text, tbLastname.Text, tbAddress.Text, tbZIP.Text, tbTown.Text, tbPhone.Text, tbMail.Text);
                                switch(returnCode)
                                {
                                    case -1:
                                        Logging.LogWarning("Es existiert bereits ein Kunde mit diesen Daten...");
                                        MessageBox.Show("Es existiert bereits ein Kunde mit diesen Daten!", "WMan - Das Werkstatt - Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                        break;

                                    case 0:
                                        Logging.LogInfo("Der Kunde wurde erfolgreich angelegt...");
                                        MessageBox.Show("Der Kunde wurde erfolgreich angelegt.", "WMan - Das Werkstatt - Management Tool", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        Close();
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
