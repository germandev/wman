﻿namespace WMan_Werkstatt_Management
{
    partial class wndCstOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(wndCstOverview));
            this.tbCstData = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCstOpen = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbLastVisit = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpBill = new System.Windows.Forms.TabPage();
            this.tpCars = new System.Windows.Forms.TabPage();
            this.tpVisits = new System.Windows.Forms.TabPage();
            this.resultCars = new System.Windows.Forms.DataGridView();
            this.cPlate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cManufactur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTyp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTuev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultWorkshop = new System.Windows.Forms.DataGridView();
            this.cDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultBills = new System.Windows.Forms.DataGridView();
            this.cBillDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPayedUntil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPayDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpBill.SuspendLayout();
            this.tpCars.SuspendLayout();
            this.tpVisits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultCars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultWorkshop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBills)).BeginInit();
            this.SuspendLayout();
            // 
            // tbCstData
            // 
            this.tbCstData.BackColor = System.Drawing.SystemColors.Control;
            this.tbCstData.Location = new System.Drawing.Point(6, 19);
            this.tbCstData.Multiline = true;
            this.tbCstData.Name = "tbCstData";
            this.tbCstData.ReadOnly = true;
            this.tbCstData.Size = new System.Drawing.Size(278, 72);
            this.tbCstData.TabIndex = 0;
            this.tbCstData.Text = "Maximilian Mustermann\r\nMusterdorfstr. 12\r\n\r\n01234 Musterhausen";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbLastVisit);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbCstOpen);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbCstData);
            this.groupBox1.Location = new System.Drawing.Point(6, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 102);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kundendaten";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(290, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Summe (offen):";
            // 
            // tbCstOpen
            // 
            this.tbCstOpen.BackColor = System.Drawing.SystemColors.Control;
            this.tbCstOpen.Location = new System.Drawing.Point(293, 32);
            this.tbCstOpen.Name = "tbCstOpen";
            this.tbCstOpen.ReadOnly = true;
            this.tbCstOpen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCstOpen.Size = new System.Drawing.Size(75, 20);
            this.tbCstOpen.TabIndex = 2;
            this.tbCstOpen.Text = "158,78 €";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Letzter Besuch:";
            // 
            // tbLastVisit
            // 
            this.tbLastVisit.BackColor = System.Drawing.SystemColors.Control;
            this.tbLastVisit.Location = new System.Drawing.Point(293, 71);
            this.tbLastVisit.Name = "tbLastVisit";
            this.tbLastVisit.ReadOnly = true;
            this.tbLastVisit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbLastVisit.Size = new System.Drawing.Size(75, 20);
            this.tbLastVisit.TabIndex = 4;
            this.tbLastVisit.Text = "01.01.1970";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpCars);
            this.tabControl1.Controls.Add(this.tpVisits);
            this.tabControl1.Controls.Add(this.tpBill);
            this.tabControl1.Location = new System.Drawing.Point(6, 110);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(584, 271);
            this.tabControl1.TabIndex = 2;
            // 
            // tpBill
            // 
            this.tpBill.Controls.Add(this.resultBills);
            this.tpBill.Location = new System.Drawing.Point(4, 22);
            this.tpBill.Name = "tpBill";
            this.tpBill.Padding = new System.Windows.Forms.Padding(3);
            this.tpBill.Size = new System.Drawing.Size(576, 245);
            this.tpBill.TabIndex = 0;
            this.tpBill.Text = "Rechnungen";
            this.tpBill.UseVisualStyleBackColor = true;
            // 
            // tpCars
            // 
            this.tpCars.Controls.Add(this.resultCars);
            this.tpCars.Location = new System.Drawing.Point(4, 22);
            this.tpCars.Name = "tpCars";
            this.tpCars.Padding = new System.Windows.Forms.Padding(3);
            this.tpCars.Size = new System.Drawing.Size(576, 245);
            this.tpCars.TabIndex = 1;
            this.tpCars.Text = "PKW";
            this.tpCars.UseVisualStyleBackColor = true;
            // 
            // tpVisits
            // 
            this.tpVisits.Controls.Add(this.resultWorkshop);
            this.tpVisits.Location = new System.Drawing.Point(4, 22);
            this.tpVisits.Name = "tpVisits";
            this.tpVisits.Size = new System.Drawing.Size(576, 245);
            this.tpVisits.TabIndex = 2;
            this.tpVisits.Text = "Werkstattbesuche";
            this.tpVisits.UseVisualStyleBackColor = true;
            // 
            // resultCars
            // 
            this.resultCars.AllowUserToAddRows = false;
            this.resultCars.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.resultCars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultCars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cPlate,
            this.cManufactur,
            this.cTyp,
            this.cTuev});
            this.resultCars.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.resultCars.Location = new System.Drawing.Point(1, 2);
            this.resultCars.Name = "resultCars";
            this.resultCars.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultCars.ShowEditingIcon = false;
            this.resultCars.Size = new System.Drawing.Size(575, 243);
            this.resultCars.TabIndex = 0;
            // 
            // cPlate
            // 
            this.cPlate.HeaderText = "Kennzeichen";
            this.cPlate.Name = "cPlate";
            this.cPlate.Width = 94;
            // 
            // cManufactur
            // 
            this.cManufactur.HeaderText = "Hersteller";
            this.cManufactur.Name = "cManufactur";
            this.cManufactur.Width = 76;
            // 
            // cTyp
            // 
            this.cTyp.HeaderText = "Typ";
            this.cTyp.Name = "cTyp";
            this.cTyp.Width = 50;
            // 
            // cTuev
            // 
            this.cTuev.HeaderText = "TÜV";
            this.cTuev.Name = "cTuev";
            this.cTuev.Width = 54;
            // 
            // resultWorkshop
            // 
            this.resultWorkshop.AllowUserToAddRows = false;
            this.resultWorkshop.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.resultWorkshop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultWorkshop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cDate,
            this.cReason});
            this.resultWorkshop.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.resultWorkshop.Location = new System.Drawing.Point(1, 1);
            this.resultWorkshop.Name = "resultWorkshop";
            this.resultWorkshop.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultWorkshop.ShowEditingIcon = false;
            this.resultWorkshop.Size = new System.Drawing.Size(575, 243);
            this.resultWorkshop.TabIndex = 1;
            // 
            // cDate
            // 
            this.cDate.HeaderText = "Datum";
            this.cDate.Name = "cDate";
            // 
            // cReason
            // 
            this.cReason.HeaderText = "Grund";
            this.cReason.Name = "cReason";
            // 
            // resultBills
            // 
            this.resultBills.AllowUserToAddRows = false;
            this.resultBills.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.resultBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultBills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cBillDate,
            this.cPayedUntil,
            this.cSum,
            this.cPayDate});
            this.resultBills.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.resultBills.Location = new System.Drawing.Point(1, 1);
            this.resultBills.Name = "resultBills";
            this.resultBills.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultBills.ShowEditingIcon = false;
            this.resultBills.Size = new System.Drawing.Size(575, 243);
            this.resultBills.TabIndex = 2;
            // 
            // cBillDate
            // 
            this.cBillDate.HeaderText = "Datum";
            this.cBillDate.Name = "cBillDate";
            // 
            // cPayedUntil
            // 
            this.cPayedUntil.HeaderText = "Fällig bis";
            this.cPayedUntil.Name = "cPayedUntil";
            // 
            // cSum
            // 
            this.cSum.HeaderText = "Betrag";
            this.cSum.Name = "cSum";
            // 
            // cPayDate
            // 
            this.cPayDate.HeaderText = "Bezahlt am";
            this.cPayDate.Name = "cPayDate";
            // 
            // wndCstOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 385);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "wndCstOverview";
            this.Text = "Kundenübersicht";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpBill.ResumeLayout(false);
            this.tpCars.ResumeLayout(false);
            this.tpVisits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resultCars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultWorkshop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBills)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbCstData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbLastVisit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCstOpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpCars;
        private System.Windows.Forms.TabPage tpVisits;
        private System.Windows.Forms.TabPage tpBill;
        private System.Windows.Forms.DataGridView resultCars;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPlate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cManufactur;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTyp;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTuev;
        private System.Windows.Forms.DataGridView resultWorkshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReason;
        private System.Windows.Forms.DataGridView resultBills;
        private System.Windows.Forms.DataGridViewTextBoxColumn cBillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPayedUntil;
        private System.Windows.Forms.DataGridViewTextBoxColumn cSum;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPayDate;
    }
}