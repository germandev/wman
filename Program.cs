﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WMan_Werkstatt_Management
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Logging.LogInfo("Programmstart...");
            Console.Title = "WMAN - Log";
            Logging.LogInfo("Öffne Datenbankverbindung...");
            sqlLite.Init();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Application.Run(new wndMain());
            }
            catch (Exception ex)
            {
                Logging.LogError(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
