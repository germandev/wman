﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace WMan_Werkstatt_Management
{
    public partial class wndCstOverview : Form
    {
        int cstID;
        public wndCstOverview(int cstNumber)
        {
            InitializeComponent();
            cstID = cstNumber;
            Logging.LogInfo("Lade Kundendaten....");
            List<string[]> currentCstList = sqlLite.searchCustomer("", "", "", cstID);
            string[] currentCst = currentCstList[0];
            tbCstData.Text = string.Format("{0} {1}\r\n{2}\r\n\r\n{3} {4}", currentCstList[0][1], currentCstList[0][0], currentCstList[0][2], currentCstList[0][3], currentCstList[0][4]);
            Logging.LogInfo("Kundendaten vollständig geladen...");
        }
    }
}
