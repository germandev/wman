﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMan_Werkstatt_Management
{
    class Logging
    {
        public static string lastMessage { private set; get; } = string.Empty;

        public static void LogError(string message)
        {
            lastMessage = string.Format("[{0}] [FEHLER]: {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), message);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(lastMessage);
        }

        public static void LogInfo(string message)
        {
            lastMessage = string.Format("[{0}] [INFO]: {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(lastMessage);
        }

        public static void LogWarning(string message)
        {
            lastMessage = string.Format("[{0}] [WARNUNG]: {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), message);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(lastMessage);
        }
    }
}
