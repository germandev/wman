﻿using System;
using System.Windows.Forms;

namespace WMan_Werkstatt_Management
{
    public partial class wndMain : Form
    {
        public wndMain()
        {
            InitializeComponent();
        }

        private void anlegenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logging.LogInfo("Erstelle \"Kundenanlage-Fenster\"...");
            wndNewCst frmNewCst = new wndNewCst();
            Logging.LogInfo("Setze Elternfenster...");
            frmNewCst.MdiParent = this;
            Logging.LogInfo("Zeige \"Kundenanlage-Fenster\" an...");
            frmNewCst.Show();
        }

        private void suchenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logging.LogInfo("Erstelle \"Kundensuche-Fenster\"...");
            wndCstSearch frmSearch = new wndCstSearch(this);
            Logging.LogInfo("Setze Elternfenster...");
            frmSearch.MdiParent = this;
            Logging.LogInfo("Zeige \"Kundensuche-Fenster\" an...");
            frmSearch.Show();
        }

        private void logTimer_Tick(object sender, EventArgs e)
        {
            tslInfo.Text = Logging.lastMessage;
        }
    }
}
